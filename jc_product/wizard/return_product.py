
from openerp import api, fields, models, _
from openerp.exceptions import UserError


class WizardScrapMove(models.TransientModel):
    _inherit = 'stock.move.scrap'

    used = fields.Selection([('yes', 'Yes'), ('no', 'No')], string='Usable', default='yes', required=True)
    product_weight = fields.Float(string='Weight', required=True)

    @api.multi
    def move_scrap(self):
        res = super(WizardScrapMove, self).move_scrap()
        # ston
        if self._context.get('active_model') == 'stone.lot.issue':
            lot_id = self.env['stone.lot.issue'].browse(self._context.get('active_id'))
            if self.product_qty > lot_id.lot_detail_lines[0].product_qty:
                raise UserError(_('Sorry, You Can not Return more then lot quantity.'))
            lot_id.lot_detail_lines.create({
                    'lot_id': lot_id.id,
                    'product_id': self.product_id.id,
                    'product_qty': -self.product_qty,
                    'product_weight': self.product_weight,
                    'assigned': True,
                    'is_returned': True,
                    })
            # return product line creation
            self.env['damage.product'].create({
                'product_usable': self.used,
                'product_type': 'stone',
                'product_id': self.product_id.id,
                'product_qty': self.product_qty,
                'product_weight': self.product_weight,
                'ref_ston': lot_id.id,
            })

        # metal
        if self._context.get('active_model') == 'metal.issue':
            metal_id = self.env['metal.issue'].browse(self._context.get('active_id'))
            if self.product_qty > metal_id.metal_detail_lines[0].product_qty:
                raise UserError(_('Sorry, You Can not Return more then lot quantity.'))
            metal_id.metal_detail_lines.create({
                    'metal_id': metal_id.id,
                    'product_id': self.product_id.id,
                    'product_qty': -self.product_qty,
                    'product_weight': self.product_weight,
                    'assigned': True,
                    'is_returned': True,
                    })
            # return product line creation
            self.env['damage.product'].create({
                'product_usable': self.used,
                'product_type': 'metal',
                'product_id': self.product_id.id,
                'product_qty': self.product_qty,
                'product_weight': self.product_weight,
                'ref_metal': metal_id.id,
            })

        # finding
        if self._context.get('active_model') == 'finding.issue':
            finding_id = self.env['finding.issue'].browse(self._context.get('active_id'))
            if self.product_qty > finding_id.finding_detail_lines[0].product_qty:
                raise UserError(_('Sorry, You Can not Return more then lot quantity.'))
            finding_id.finding_detail_lines.create({
                'finding_id': finding_id.id,
                'product_id': self.product_id.id,
                'product_qty': -self.product_qty,
                'product_weight': self.product_weight,
                'assigned': True,
                'is_returned': True,
            })
            # return product line creation
            self.env['damage.product'].create({
                'product_usable': self.used,
                'product_type': 'finding',
                'product_id': self.product_id.id,
                'product_qty': self.product_qty,
                'product_weight': self.product_weight,
                'ref_finding': finding_id.id,
            })

        # rhodium
        if self._context.get('active_model') == 'rhodium.issue':
            rhodium_id = self.env['rhodium.issue'].browse(self._context.get('active_id'))
            if self.product_qty > rhodium_id.rhodium_detail_lines[0].product_qty:
                raise UserError(_('Sorry, You Can not Return more then lot quantity.'))
            rhodium_id.rhodium_detail_lines.create({
                'rhodium_id': rhodium_id.id,
                'product_id': self.product_id.id,
                'product_qty': -self.product_qty,
                'product_weight': self.product_weight,
                'assigned': True,
                'is_returned': True,
            })
            # return product line creation
            self.env['damage.product'].create({
                'product_usable': self.used,
                'product_type': 'rhodium',
                'product_id': self.product_id.id,
                'product_qty': self.product_qty,
                'product_weight': self.product_weight,
                'ref_rhodium': rhodium_id.id,
            })
        return res
