from openerp import models, fields, api, _
from openerp.exceptions import UserError

class project_project_produce(models.TransientModel):
    _name = "project.project.produce"
    _description = "Project Produce"

    project_id = fields.Many2one('project.project', string="Department")

    @api.multi
    def do_produce(self):
        production_id = self.env['mrp.production'].browse(self._context.get('active_id'))
        if not self.project_id:
            raise UserError(_('Sorry, You Can not Start Production without selection Starting Department.'))
        self.env['project.task'].create({
            'name': production_id.product_id.id,
            'production_id': production_id.id,
            'project_id': self.project_id.id,
            'issue_wt': production_id.product_id.master_weight * production_id.product_qty,
            })
        production_id.state = 'in_production'
        production_id.date_start = fields.datetime.now()


class project_task_wiz_dept(models.TransientModel):
    _name = "project.task.wiz.dept"

    project_id = fields.Many2one('project.project', string="Next Department", required=True)
    weight = fields.Float('Weight', required=True)

    @api.multi
    def do_produce(self):
        task_id = self.env['project.task'].browse(self._context.get('active_id'))
        line = task_id.project_task_history_dept_ids[-1]
        if self.weight > line.receive_wt:
            raise UserError('Sorry, You can not send more then Recived weight')
            self.env['project.task'].browse(self._context.get('active_id')).project_task_history_dept_ids[-1]
        line.done_date = fields.datetime.now()
        line.done_weight = self.weight
        task_id.project_id = self.project_id.id

    @api.model
    def default_get(self, fields):
        res = super(project_task_wiz_dept, self).default_get(fields)
        task = self.env['project.task'].browse(self._context.get('active_id'))
        res['weight'] = task.project_task_history_dept_ids[-1].receive_wt
        return res

    @api.onchange('product_id')
    def _change_product_id(self):
        task = self.env['project.task'].browse(self._context.get('active_id'))
        project = self.env['project.project'].search([('id', '!=', task.project_id.id)]).ids
        return {'domain': {'project_id': [('id', 'in', project)]}}
