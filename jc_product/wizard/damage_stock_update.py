
from openerp import models, fields, api, _
from openerp.exceptions import UserError
# -------------- Damage Product ---------------

class WizardUpdateDamageProduct(models.TransientModel):
    _name = 'update.damage.product'

    product_type = fields.Selection([
        ('metal', 'Metal'),
        ('stone', 'Stone'),
        ('finding', 'Finding')], string='Type', readonly=True)
    product_id = fields.Many2one('product.product', string='Product', required=True)
    product_qty = fields.Float(string='Quantity', required=True)
    product_uom_id = fields.Many2one('product.uom', related='product_id.uom_id', string='Unit of Measure', required=True)
    product_weight = fields.Float(string='Weight', required=True)
    ref_ston = fields.Many2one('stone.lot.issue', 'Stone Lot Reference')
    ref_metal = fields.Many2one('metal.issue', 'Metal Reference')
    ref_finding = fields.Many2one('finding.issue', 'Findings Reference')

    @api.model
    def default_get(self, fields):
        res = super(WizardUpdateDamageProduct, self).default_get(fields)
        lot = self._context.get('active_id')
        res['product_type'] = self.env['damage.product'].browse(self._context.get('active_id')).product_type
        return res

    @api.onchange('product_id')
    def _change_product_id(self):
        return {'domain': {'product_id': [('product_type', '=', self.product_type)]}}
