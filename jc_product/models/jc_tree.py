
from openerp import models, fields, api

class DepartmentTaskTree(models.Model):
    _name = 'dept.tree.task'
    _rec_name = 'name'

    _inherit = ['mail.thread', 'ir.needaction_mixin']

    name = fields.Char(string='Tree Number', required=True, readonly=True, copy=False, default='New')
    tree_detail_lines = fields.One2many('dept.task.lines', 'tree_id', string="Tree Detail")
    state = fields.Selection(
            [('draft', 'New'), ('inprocess', 'In Process'), ('used', 'Used')],
            string='Status', copy=False, default='draft')
    assign_to = fields.Many2one('hr.employee', string='Assigned to',  track_visibility='onchange')
    _sql_constraints = [
        ('name_uniq', 'unique(name)', 'Tree Number must be unique.'),
    ]

    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            vals['name'] = self.env['ir.sequence'].next_by_code('dept.tree.task') or 'New'
        result = super(DepartmentTaskTree, self).create(vals)
        return result


class DepartmentTaskLines(models.Model):
    _name = 'dept.task.lines'
    tree_id = fields.Many2one('dept.tree.task', string="Tree Number")
    task_id = fields.Many2one('project.task', string="Task", required=True)
    product_id = fields.Many2one('product.product', string='Product', required=True)
    product_qty = fields.Float(string='Quantity', required=True)
    product_uom_id = fields.Many2one('product.uom', related='product_id.uom_id', string='Unit of Measure', required=True)
    total_weight = fields.Float(string="Total Weight")

    @api.onchange('product_qty', 'product_id')
    def _onchange_product(self):
        self.total_weight = self.product_qty * self.product_id.master_weight
