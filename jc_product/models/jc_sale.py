
from openerp import models, fields, api

class product_product(models.Model):
    _inherit = "sale.order.line"
    pro_code = fields.Many2one('product.code', string='Product Code', change_default=True)
    image_small = fields.Binary('Product Image',
                                related='product_id.image_small')
    master_size = fields.Float(string='Size', related='product_id.master_size')
    master_weight = fields.Float(string='Weight', related='product_id.master_weight')
    total_weight = fields.Float(string="Total Weight")

    @api.one
    @api.onchange('pro_code', 'product_id')
    def product_change(self):
        if self.product_id:
            if self.product_id.product_code:
                self.pro_code = self.product_id.product_code[0].id
            else:
                self.pro_code = ''
        if self.pro_code:
            self.product_id = self.pro_code.code_product.id

    @api.one
    @api.onchange('product_uom_qty', 'product_id')
    def product_qty_change(self):
        self.total_weight = self.master_weight * self.product_uom_qty
