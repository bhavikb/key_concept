
from openerp import models, fields, api


class jc_damage_product(models.Model):
    _name = 'damage.product'
    _rec_name = "product_id"

    product_usable = fields.Selection([
        ('yes', 'Yes'),
        ('no', 'No')], 'Product Usable')
    product_type = fields.Selection([
        ('metal', 'Metal'),
        ('stone', 'Stone'),
        ('finding', 'Finding')], 'Type')
    product_id = fields.Many2one('product.product', string='Product', required=True)
    product_qty = fields.Float(string='Quantity', required=True)
    product_uom_id = fields.Many2one('product.uom', related='product_id.uom_id', string='Unit of Measure', required=True)
    product_weight = fields.Float(string='Weight', required=True)
    ref_ston = fields.Many2one('stone.lot.issue', 'Stone Lot Reference')
    ref_metal = fields.Many2one('metal.issue', 'Metal Reference')
    ref_finding = fields.Many2one('finding.issue', 'Findings Reference')
    ref_rhodium = fields.Many2one('rhodium.issue', 'Rhodium Reference')


    @api.multi
    def button_stock_update(self):
        form_view_id = self.env['ir.model.data'].get_object_reference('jc_product', 'wiz_damage_stock_update_form_view')[1]
        return {
                'type': 'ir.actions.act_window',
                'name': 'Update Damage Product',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'update.damage.product',
                'views': [(form_view_id, 'form')],
                'target': 'new',
                'context': self.id,
            }
