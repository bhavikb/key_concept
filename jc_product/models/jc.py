
from openerp import models, fields, api, _
from openerp.exceptions import UserError
# -------------- STONE ---------------
class stone_issue(models.Model):
    _name = 'stone.lot.issue'
    _inherit = ['mail.thread', 'ir.needaction_mixin']

    name = fields.Char(string='Lot Number', required=True, readonly=True, copy=False, default="New")
    assign_to = fields.Many2one('hr.employee', string='Assigned to', track_visibility='onchange')
    to_dept = fields.Many2one('project.project', string='To Department', track_visibility='onchange')
    state = fields.Selection(
            [('draft', 'New'), ('assigned', 'Assigned'), ('cancel', 'Cancelled')],
            string='Status', copy=False, default='draft')
    lot_detail_lines = fields.One2many('lot.detail', 'lot_id', string='Lot Detail')
    production_id = fields.Many2one('mrp.production', 'Manufacturing Order',
            select=True, ondelete='cascade', required=True)
    date_planned = fields.Datetime(string='Scheduled Date', required=True, select=1, copy=False)
    _sql_constraints = [
        ('name_uniq', 'unique(name)', 'Lot Number must be unique.'),
    ]

    @api.one
    def button_assign(self):
        if not self.assign_to:
            raise UserError(_('Before make it assign Please select Employee.'))
        self.state = 'assigned'

    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            vals['name'] = self.env['ir.sequence'].next_by_code('stone.lot.issue') or 'New'
        result = super(stone_issue, self).create(vals)
        return result

class lot_detail(models.Model):
    _name = 'lot.detail'

    lot_id = fields.Many2one('stone.lot.issue', string='Lot Number')
    product_id = fields.Many2one('product.product', string='Product', required=True, domain=[('product_type', '=', 'stone')])
    product_qty = fields.Float(string='Quantity', required=True)
    product_uom_id = fields.Many2one('product.uom', related='product_id.uom_id', string='Unit of Measure', required=True)
    product_weight = fields.Float(string='Weight', required=True)
    assigned = fields.Boolean('Assigned Lot')
    is_returned = fields.Boolean('Returned')
    removed = fields.Boolean('Stock Removed')

# ----------- METAL -------------
class metal_issue(models.Model):
    _name = 'metal.issue'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    name = fields.Char(string='Metal Number', required=True, readonly=True, copy=False, default="New")
    assign_to = fields.Many2one('hr.employee', string='Assigned to', track_visibility='onchange')
    to_dept = fields.Many2one('project.project', string='To Department', track_visibility='onchange')
    state = fields.Selection(
            [('draft', 'New'), ('assigned', 'Assigned'), ('cancel', 'Cancelled')],
            string='Status', copy=False, default='draft')
    metal_detail_lines = fields.One2many('metal.detail', 'metal_id', string='Lot Detail')
    production_id = fields.Many2one('mrp.production', 'Manufacturing Order',
            select=True, ondelete='cascade', required=True)
    date_planned = fields.Datetime(string='Scheduled Date', required=True, select=1, copy=False)
    _sql_constraints = [
        ('name_uniq', 'unique(name)', 'metal Number must be unique.'),
    ]

    @api.one
    def button_assign_metal(self):
        if not self.assign_to:
            raise UserError(_('Before make it assign Please select Employee.'))
        self.state = 'assigned'

    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            vals['name'] = self.env['ir.sequence'].next_by_code('metal.issue') or 'New'
        result = super(metal_issue, self).create(vals)
        return result

    # @api.multi
    # def return_product_wiz_call_metal(self):
    #     form_view_id = self.env['ir.model.data'].get_object_reference('jc_product', 'wiz_return_product_metal_form_view')[1]
    #     return {
    #             'type': 'ir.actions.act_window',
    #             'name': 'Return Metal',
    #             'view_type': 'form',
    #             'view_mode': 'form',
    #             'res_model': 'return.product.metal',
    #             'views': [(form_view_id, 'form')],
    #             'target': 'new',
    #             'context': self.id,
    #         }

class metal_detail(models.Model):
    _name = 'metal.detail'

    metal_id = fields.Many2one('metal.issue', string='metal Number')
    product_id = fields.Many2one('product.product', string='Product', required=True, domain=[('product_type', '=', 'metal')])
    product_qty = fields.Float(string='Quantity', required=True)
    product_uom_id = fields.Many2one('product.uom', related='product_id.uom_id', string='Unit of Measure', required=True)
    product_weight = fields.Float(string='Weight', required=True)
    assigned = fields.Boolean('Assigned Lot')
    is_returned = fields.Boolean('Returned')
    removed = fields.Boolean('Stock Removed')

# ----------- FINDING -----------------

class finding_issue(models.Model):
    _name = 'finding.issue'
    _inherit = ['mail.thread', 'ir.needaction_mixin']

    name = fields.Char(string='Finding Number', required=True, readonly=True, copy=False, default="New")
    assign_to = fields.Many2one('hr.employee', string='Assigned to', track_visibility='onchange')
    to_dept = fields.Many2one('project.project', string='To Department', track_visibility='onchange')
    state = fields.Selection(
            [('draft', 'New'), ('assigned', 'Assigned'), ('cancel', 'Cancelled')],
            string='Status', copy=False, default='draft')
    finding_detail_lines = fields.One2many('finding.detail', 'finding_id', string='Lot Detail')
    production_id = fields.Many2one('mrp.production', 'Manufacturing Order',
            select=True, ondelete='cascade', required=True)
    date_planned = fields.Datetime(string='Scheduled Date', required=True, select=1, copy=False)
    _sql_constraints = [
        ('name_uniq', 'unique(name)', 'finding Number must be unique.'),
    ]

    @api.one
    def button_assign_finding(self):
        if not self.assign_to:
            raise UserError(_('Before make it assign Please select Employee.'))
        self.state = 'assigned'

    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            vals['name'] = self.env['ir.sequence'].next_by_code('finding.issue') or 'New'
        result = super(finding_issue, self).create(vals)
        return result

class finding_detail(models.Model):
    _name = 'finding.detail'

    finding_id = fields.Many2one('finding.issue', string='Finding Number')
    product_id = fields.Many2one('product.product', string='Product', required=True, domain=[('product_type', '=', 'finding')])
    product_qty = fields.Float(string='Quantity', required=True)
    product_uom_id = fields.Many2one('product.uom', related='product_id.uom_id', string='Unit of Measure', required=True)
    product_weight = fields.Float(string='Weight', required=True)
    assigned = fields.Boolean('Assigned Finding')
    is_returned = fields.Boolean('Returned')
    removed = fields.Boolean('Stock Removed')

# ----------- Rhodium -------------

class rhodium_issue(models.Model):
    _name = 'rhodium.issue'
    _inherit = ['mail.thread', 'ir.needaction_mixin']

    name = fields.Char(string='Rhodium Number', required=True, readonly=True, copy=False, default="New")
    assign_to = fields.Many2one('hr.employee', string='Assigned to', track_visibility='onchange')
    to_dept = fields.Many2one('project.project', string='To Department', track_visibility='onchange')
    state = fields.Selection(
            [('draft', 'New'), ('assigned', 'Assigned'), ('cancel', 'Cancelled')],
            string='Status', copy=False, default='draft')
    rhodium_detail_lines = fields.One2many('rhodium.detail', 'rhodium_id', string='Lot Detail')
    production_id = fields.Many2one('mrp.production', 'Manufacturing Order',
            select=True, ondelete='cascade', required=True)
    date_planned = fields.Datetime(string='Scheduled Date', required=True, select=1, copy=False)
    _sql_constraints = [
        ('name_uniq', 'unique(name)', 'Rhodium Number must be unique.'),
    ]

    @api.one
    def button_assign_rhodium(self):
        if not self.assign_to:
            raise UserError(_('Before make it assign Please select Employee.'))
        self.state = 'assigned'

    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            vals['name'] = self.env['ir.sequence'].next_by_code('rhodium.issue') or 'New'
        result = super(rhodium_issue, self).create(vals)
        return result

class rhodium_detail(models.Model):
    _name = 'rhodium.detail'

    rhodium_id = fields.Many2one('rhodium.issue', string='Rhodium Number')
    product_id = fields.Many2one('product.product', string='Product', required=True, domain=[('product_type', '=', 'rhodium')])
    product_qty = fields.Float(string='Quantity', required=True)
    product_uom_id = fields.Many2one('product.uom', related='product_id.uom_id', string='Unit of Measure', required=True)
    product_weight = fields.Float(string='Weight', required=True)
    assigned = fields.Boolean('Assigned Rhodium')
    is_returned = fields.Boolean('Returned')
    removed = fields.Boolean('Stock Removed')
