
from openerp import models, fields, api

class stock_move(models.Model):
    _inherit = "stock.move"

    assigned = fields.Boolean()

    @api.multi
    def action_assign(self):
        res = super(stock_move, self).action_assign()
    	self.assign_things()

    @api.multi
    def force_assign(self):
        res = super(stock_move, self).force_assign()
    	self.assign_things()

    @api.multi
    def action_cancel(self):
        res = super(stock_move, self).action_cancel()
    	self.assign_things_cancle()

    def assign_things(self):
        for move in self:
        	if move.raw_material_production_id:
	            details = {'product_id': move.product_id.id,
	            'product_qty': move.product_qty,
	            'product_weight': 0.0,
	            'assigned': True}
	            if move.state == 'assigned' and not move.assigned:
	            	move.assigned = True
	                if move.product_id.product_type == 'stone':
	                    self.env['stone.lot.issue'].create({
	                    	'production_id': move.raw_material_production_id.id,
	        				'date_planned': move.raw_material_production_id.date_planned,
	        				'lot_detail_lines': [(0, 0, details)]
	        				})

	                if move.product_id.product_type == 'metal':
	                    self.env['metal.issue'].create({
	                    	'production_id': move.raw_material_production_id.id,
	        				'date_planned': move.raw_material_production_id.date_planned,
	        				'metal_detail_lines': [(0, 0, details)]
	        				})

	                if move.product_id.product_type == 'finding':
	                    self.env['finding.issue'].create({
	                    	'production_id': move.raw_material_production_id.id,
	        				'date_planned': move.raw_material_production_id.date_planned,
	        				'finding_detail_lines': [(0, 0, details)]
	        				})

	                if move.product_id.product_type == 'rhodium':
	                    self.env['rhodium.issue'].create({
	                        'production_id': move.raw_material_production_id.id,
                            'date_planned': move.raw_material_production_id.date_planned,
                            'rhodium_detail_lines': [(0, 0, details)]
                            })

    def  assign_things_cancle(self):
		for move in self:
			if move.raw_material_production_id:
				move.assigned = False
				stone_id = self.env['stone.lot.issue'].search([('production_id', '=', move.raw_material_production_id.id)])
				for stone in stone_id:
					stone.state = 'cancel'
				metal_id = self.env['metal.issue'].search([('production_id', '=', move.raw_material_production_id.id)])
				for metal in metal_id:
					metal.state = 'cancel'
				finding_id = self.env['finding.issue'].search([('production_id', '=', move.raw_material_production_id.id)])
				for finding in finding_id:
					finding.state = 'cancel'
				rhodium_id = self.env['rhodium.issue'].search([('production_id', '=', move.raw_material_production_id.id)])
				for rhodium in rhodium_id:
					rhodium.state = 'cancel'
