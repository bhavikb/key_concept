
from openerp import models, fields, api, _
from openerp.exceptions import UserError
import time, datetime

# mrp Department
class ProjectDepartment(models.Model):
    _inherit = 'project.project'

    privacy_visibility = fields.Selection(default='followers')
    tree_creation = fields.Boolean('Tree Creation')

    @api.model
    def create(self, vals):
        res = super(ProjectDepartment, self).create(vals)
        res.type_ids = [
            (4, self.env.ref('jc_product.department_stage_data_0').id),
            (4, self.env.ref('jc_product.department_stage_data_1').id),
            (4, self.env.ref('jc_product.department_stage_data_2').id)]
        return res

    @api.multi
    def write(self, vals):
        res = super(ProjectDepartment, self).write(vals)
        return res

class ProjectTaskDepartment(models.Model):
    _inherit = 'project.task'

    name = fields.Many2one('product.product', required=True)
    production_id = fields.Many2one('mrp.production', 'Manufacturing Order',
            select=True, ondelete='cascade')
    tree_creation = fields.Boolean('Tree Creation',
                                related='project_id.tree_creation')
    issue_wt = fields.Float('Issue Weight')
    project_task_history_dept_ids = fields.One2many('project.task.history.dept', 'project_task_history_id', 'Project History')
    date_deadline = fields.Datetime('Issue Date', default=fields.datetime.now())

    @api.multi
    def state_change_start(self):
        project = any([self.project_id == line.issue_dept for line in self.project_task_history_dept_ids])
        weight = self.issue_wt
        if self.project_task_history_dept_ids:
            if self.project_task_history_dept_ids[-1].done_weight:
                weight = self.project_task_history_dept_ids[-1].done_weight
        if not self.project_task_history_dept_ids or not project:
            self.project_task_history_dept_ids = [(0, 0, {'issue_dept': self.project_id.id, 'start_date': fields.datetime.now(), 'receive_wt': weight})]
        # last = self.project_task_history_dept_ids[-1]
        return True

class ProjectTaskHistory(models.Model):
    _name = 'project.task.history.dept'

    project_task_history_id = fields.Many2one('project.task')
    issue_dept = fields.Many2one('project.project', 'Issue Department')
    start_date = fields.Datetime('Start Date')
    done_date = fields.Datetime('Done Date')
    receive_wt = fields.Float('Receive Weight')
    done_weight = fields.Float('Done Weight')
