from openerp import models, fields, api

class product_image(models.Model):
    _name = "product.image"

    image = fields.Binary('Image')
    name = fields.Char('Image Name')
    product_id = fields.Many2one('product.template', 'Product')

class product_product(models.Model):
    _inherit = "product.product"
    product_code = fields.One2many('product.code', 'code_product', string='Product Code')
    barcode_13 = fields.Char(string='Barcode', readonly=True)

    @api.model
    def create(self, vals):
        res = super(product_product, self).create(vals)
        if not res.barcode_13:
            res.barcode_13 = res.id
        return res

class product_template(models.Model):
    _inherit = "product.template"

    product_code = fields.Char(string='Product Code')
    date = fields.Date(string='Date')
    description = fields.Char(string='Description')
    jewel_code = fields.Many2one('product.jewel', string='Jewel Code')
    design_code = fields.Char(string='Design No')
    workman = fields.Many2one('hr.employee', string='Workman')
    customer = fields.Many2one('res.partner', string='Customer', select=True, domain=[('customer', '=', True)])
    metal_code = fields.Many2one('product.metal', string='Metal')
    master_size = fields.Float(string='Master Size')

    # code = fields.One2many('product.jewel', 'code_jewel', string='Code')
    master_weight = fields.Float(string='Master Weight')
    parts = fields.Integer(string='Parts')
    length_mm = fields.Float(string='Length (MM)')
    weight_mm = fields.Float(string='Weight (MM)')
    height_mm = fields.Float(string='Height (MM)')

    length_inch = fields.Float(string='Length (Inch)')
    widht_inch = fields.Float(string='Width (Inch)')
    height_inch = fields.Float(string='Height (Inch)')

    shank_width_mm = fields.Float(string='Shank Width (MM)')
    shank_width_inch = fields.Float(string='Shank Width (Inch)')
    wax_weight = fields.Float(string='Wax Weight')
    link_type = fields.Char(string='link Typeo')

    size = fields.Float(string='Size')
    with_sprue = fields.Float(string='With Sprue')
    clasp = fields.Many2one('product.template', string='Clasp', domain=[('product_type', '=', 'finding')])
    remark = fields.Text(string='Remark')

    product_type = fields.Selection([
        ('metal', 'Metal'),
        ('stone', 'Stone'),
        ('finding', 'Finding'),
        ('rhodium', 'Rhodium')], 'Type')
    # multi static images for product
    image1 = fields.Binary(string='Image')
    image2 = fields.Binary(string='Image')
    image3 = fields.Binary(string='Image')
    image4 = fields.Binary(string='Image')
    # multi image in list view
    images_ids = fields.One2many('product.image', 'product_id', 'Images')

    #product with diffrent code
    # product_code = fields.One2many('product.code', 'code_product', string='Product Code')

    _sql_constraints = [
        ('product_name_uniq', 'unique(name)', 'Product Code must be unique !'),
    ]

    @api.model
    def create(self, vals):
        res = super(product_template, self).create(vals)
        if res.description:
            res.description_sale = res.description
            if not res.description_purchase:
                res.description_purchase = res.description
        return res

    @api.multi
    def write(self, vals):
        res = super(product_template, self).write(vals)
        if vals.get('description'):
            self.description_sale = vals.get('description')
            if not self.description_purchase:
                self.description_purchase = vals.get('description')
        return res

class product_workmen(models.Model):
    _name = "product.workman"
    name = fields.Char(string='Name')

class product_jewel(models.Model):
    _name = "product.jewel"
    name = fields.Char(string='Name')
    date = fields.Date(string='Date')

class product_metal(models.Model):
    _name = "product.metal"
    _rec_name = "metal_type"
    metal_type = fields.Char(string='Metal Type')

class product_source_code(models.Model):
    _name = "product.code"
    _rec_name = 'code'

    code = fields.Char(string='Code')
    product_source_id = fields.Many2one('product.name', string='Name')
    code_product = fields.Many2one('product.product', string='Code')

class product_source(models.Model):
    _name = "product.name"
    name = fields.Char(string='Source')
