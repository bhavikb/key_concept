
from openerp import models, fields, api

class purchase_order(models.Model):
    _inherit = "stock.picking"
    add_less_line = fields.One2many('picking.add.less.weight', 'picking_id', string='Add / Less')

class purchase_add_less_weight(models.Model):
    _name = "picking.add.less.weight"

    @api.onchange('product_id')
    def _product_domain(self):
        product_domain = []
        if self.picking_id.pack_operation_product_ids:
            for order in self.picking_id.pack_operation_product_ids:
                for product in order.product_id:
                    product_domain.append(product.id)
        return {'domain': {'product_id': [('id', 'in', product_domain)]}}

    picking_id = fields.Many2one('stock.picking', string='Add / Less')
    product_id = fields.Many2one('product.product', string='Product', required=True)
    add_loss = fields.Selection([
        ('add', 'Add'),
        ('loss', 'loss'),
        ], string='Add / Loss', default='add', required=True)
    product_qty = fields.Float(string='Quantity', required=True)
    product_uom_id = fields.Many2one('product.uom', related='product_id.uom_id', string='Unit of Measure', required=True)
