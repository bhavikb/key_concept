# -*- encoding: UTF-8 -*-
##############################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2015-Today Key Concepts IT Services LLP.
#    (<http://keyconcepts.co.in>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################

{
    "name": "jewellery",
    "version": "9.0",
    'author': 'Key Concepts IT Services LLP.',
    'website': 'http://keyconcepts.co.in',
    "category": "Jewellery",
    "description": """
        A Specific Module for JC which will make the use of Product module..
    """,
    "depends": [
        "project",
        "jc_web_tree_image",
        "sale",
        "mrp",
        "purchase",
        "hr",
    ],
    "data": [
        "data/data.xml",
        "security/jc_product_security.xml",
        "security/ir.model.access.csv",
        "views/jc_product_view.xml",
        "views/jc_sale_view.xml",
        "views/jc_view.xml",
        "views/jc_damage_product_view.xml",
        "wizard/jc_task_done_wizard_view.xml",
        "views/jc_mrp_view.xml",
        # "views/jc_purchase_view.xml",
        "wizard/return_product_view.xml",
        "wizard/damage_stock_update_view.xml",
        "views/jc_department_view.xml",
        "views/jc_tree_view.xml",
        ],
    "demo": [],
    "auto_install": False,
    "installable": True,
    'certificate': '',
}
